# Cache simulator IE521 project baseline
Link al repo:
https://gitlab.com/fabian.guerra/tarea2_estructuras_compus_2_entrega


Instrucciones para simular:
  cd /build/src

  make l1cache

  gunzip -c /path/to/trace.gz | ./cachel1 -t <#> -a <#> -l <#> -rp <#>
  
  ej:

  gunzip -c ../../src/art.trace.gz | ./cachel1 -t 32 -a 8 -l 64 -rp 2




Code base line for IE0521 cache simulation project


## How to build the project
Create a build directory and run all targets there
```
>> mkdir build
>> cd build
>> cmake ..
>> make <target> (l1cache or cachetest)
```
## How to run the tests
Go to build and make cachetest. There are several options to run the tests.

1. Run all tests:
```
  ./test/cachetest
```
2. Run only one test:
```
  ./test/cachetest  --gtest_filter=<test_name>
  Ex: ./test/cachetest  --gtest_filter=L1cache.hit_miss_srrip
  Ex: ./test/cachetest  --gtest_filter=L1cache.hit_miss_lru
  Ex: ./test/cachetest  --gtest_filter=L1cache.hit_miss_nru
  Ex: ./test/cachetest  --gtest_filter=L1cache.promotion
  Ex: ./test/cachetest  --gtest_filter=L1cache.writeback
  Ex: ./test/cachetest  --gtest_filter=L1cache.boundaries
```
3. Run a test n times:
```
./test/cachetest  --gtest_filter=<test_name> --gtest_repeat=<n>
Ex: ./test/cachetest  --gtest_filter=L1cache.hit_miss_srrip --gtest_repeat=2
```
4. Replicate test results:
```
  Each test is run with base seed, to replicate the result the same seed must be used
  ./test/cachetest  --gtest_filter=<test_name> --gtest_random_seed=<test_seed>
  ./test/cachetest  --gtest_filter=L1cache.hit_miss_srrip --gtest_random_seed=2126
```  
To enable further debug on the test you need to enable debug_on variable, on your terminal
do:
```
export TEST_DEBUG=1
```
To disable the extra logging, set the  environment variable to zero.

## How to run the simulation
The simulation executable is located inside the build directory (src/l1cache)
```
gunzip -c <trace> | <l1cache executable>  -a <associativity> -mp -s <cache size KB> -l <block size in bytes> -rp <replacement policy>
```

### Dependencies
Make sure gtest is install:
```
sudo apt-get install libgtest-dev

sudo apt-get install cmake # install cmake
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make
```
