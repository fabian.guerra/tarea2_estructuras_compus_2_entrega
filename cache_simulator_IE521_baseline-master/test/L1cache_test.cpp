/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#define YEL   "\x1B[33m"

/* Globals */
int debug_on = 0;

/* Test Helpers */
#define DEBUG(x) if (debug_on) printf("%s\n",#x)
/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = rand()%2;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST(L1cache, promotion){
  int status;
  int i;
  int idx;
  int tag;
  int tag2;
  int associativity;
  int hit_way;
  int replacement_policy_rand = rand()%3;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  bool hay_eviction = false;
  struct operation_result result = {};
  int expected_rp_value;

  /* Fill a random cache entry */
  idx = rand()%256;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(Fill cache line);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      //valores de rp_value iniciales
      if (replacement_policy_rand == 0){
        cache_line[i].rp_value = associativity-1-i; 
      }
      if (replacement_policy_rand == 1){
        cache_line[i].rp_value = rand()%2;
      }
      if (replacement_policy_rand == 2){
        cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      }
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    DEBUG(force hit);
    if (replacement_policy_rand == 0){
      // dos iteraciones para forzar un hit
      status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      hit_way = result.evicted_address;                                 
    }
    if (replacement_policy_rand == 1){
      status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));   
      hit_way = result.evicted_address;                                  
    }
    if (replacement_policy_rand == 2){
      status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      hit_way = result.evicted_address;  
    }
    expected_rp_value = 0;
    EXPECT_EQ(cache_line[hit_way].rp_value, expected_rp_value);
    //tengo un bloque nuevo en cache_line[hit_way] con valor de tag = tag
    tag2 = rand()%4096;
    while (tag2 == tag){tag2 = rand()%4096;} // tag2 diferente de tag para ingresar N veces tag
    // ingreso 'asociatividad' veces un bloque y asi se asegura que sale del bloque

    for(int x = 0; x < associativity; x++){
      if(replacement_policy_rand == 0){
        status = lru_replacement_policy(idx, 
                                      tag2, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));        
      }
      if(replacement_policy_rand == 0){
        status = nru_replacement_policy(idx, 
                                      tag2, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));        
      }
      if(replacement_policy_rand == 0){
        status = srrip_replacement_policy(idx, 
                                      tag2, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      bool(debug_on));        
      }
    }
    if((result.evicted_address == tag) && (hay_eviction == false)){
      hay_eviction = true;
      EXPECT_EQ(hay_eviction, true);
    }


    
    
  }
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST(L1cache, writeback){
  int status = 0;
  int idx = 0;
  int tag = 0;
  int rp = rand()%3;
  int associativity = 1 << (rand()%4);
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 0;
  bool debug = 0;
  struct operation_result result = {};

  struct entry cache_line[2][associativity];

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < associativity; j++) {
      cache_line[i][j].valid = true;
      cache_line[i][j].dirty = false;
      cache_line[i][j].tag = 0;
      if (rp == LRU) {
      cache_line[i][j].rp_value = associativity-j-1;
      }
      if (rp == NRU) {
      cache_line[i][j].rp_value = 1;
      }
      if (rp == RRIP) {
      cache_line[i][j].rp_value = (associativity <= 2)? 1:3;
      }
    }
  }
  
  int tag_array[associativity];
  tag_array[0] = rand()%4096;
  printf("RP: %i \n", rp);
  printf("ASSOCIATI: %i \n", associativity);
  printf("INDEX: %i \n", idx);
  printf("TAG: %i \n", tag_array[0]);
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < associativity; j++) {
      
      tag_array[j] = tag_array[0] + j; //Se hace que los tags del bloque sean consecutivos
      
      if (rp == LRU) { //LRU
        status = lru_replacement_policy(idx, 
                                     tag_array[j], 
                                     associativity,
                                     loadstore,
                                     cache_line[i],
                                     &result,
                                     bool(debug_on));

      } else {
        if (rp == NRU) { //En caso de usarse rp NRU
          status = nru_replacement_policy(idx, 
                                     tag_array[j], 
                                     associativity,
                                     loadstore,
                                     cache_line[i],
                                     &result,
                                     bool(debug_on));
        } else {
          if (rp == RRIP) { //RRIP
            status = srrip_replacement_policy(idx, 
                                     tag_array[j], 
                                     associativity,
                                     loadstore,
                                     cache_line[i],
                                     &result,
                                     bool(debug_on));
          } 
        }
      }

      expected_miss_hit = MISS_LOAD;
        EXPECT_EQ(result.miss_hit, expected_miss_hit);
        EXPECT_EQ(status, 0);        
    }
  }
  
  //Se randomiza el set
  int setA = rand()%2;
  int setB;
  if (setA == 1) {
    setB = 0;
  } else {
    setB = 1;
  }

  //Se randomiza el block del set donde se tendrá el hit
  int blockA;
  blockA = rand()%associativity;

  loadstore = 1;

  if (rp == LRU) { //LRU
    status = lru_replacement_policy(idx, 
                                     tag_array[blockA], 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));

  } else {
    if (rp == NRU) { //En caso de usarse rp NRU
      status = nru_replacement_policy(idx, 
                                     tag_array[blockA], 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));
    } else {
      if (rp == RRIP) { //RRIP
        status = srrip_replacement_policy(idx, 
                                     tag_array[blockA], 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));
      }
    }
  }
  printf("Block: %i \n", blockA);
  printf("Seta: %i \n", setA);
  printf("Tag in: %i \n", tag_array[blockA]);
  printf("Tag dest: %i \n", cache_line[setA][blockA].tag);
  printf("Tag destA: %i \n", cache_line[setA][blockA].tag);
  expected_miss_hit = HIT_STORE;
  EXPECT_EQ(result.miss_hit, expected_miss_hit); 
  EXPECT_EQ(status, 0);
  EXPECT_EQ(cache_line[setA][blockA].dirty, 1);

  //Se randomiza el bloque que se leera, esta vez en el set contrario
  int blockB = rand()%associativity;
  loadstore = 0;

  if (rp == LRU) { //LRU
    status = lru_replacement_policy(idx, 
                                     tag_array[blockB], 
                                     associativity,
                                     loadstore,
                                     cache_line[setB],
                                     &result,
                                     bool(debug_on));

  } else {
    if (rp == NRU) { //En caso de usarse rp NRU
      status = nru_replacement_policy(idx, 
                                     tag_array[blockB], 
                                     associativity,
                                     loadstore,
                                     cache_line[setB],
                                     &result,
                                     bool(debug_on));
    } else {
      if (rp == RRIP) { //RRIP
        status = srrip_replacement_policy(idx, 
                                     tag_array[blockB], 
                                     associativity,
                                     loadstore,
                                     cache_line[setB],
                                     &result,
                                     bool(debug_on));
      }
    }
  }
  expected_miss_hit = HIT_LOAD;
  EXPECT_EQ(result.miss_hit, expected_miss_hit); 
  EXPECT_EQ(status, 0);

  //Se randomiza el bloque que se leera, esta vez en el set contrario
  loadstore = 0;

  if (rp == LRU) { //LRU
    status = lru_replacement_policy(idx, 
                                     tag_array[blockA], 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));

  } else {
    if (rp == NRU) { //En caso de usarse rp NRU
      status = nru_replacement_policy(idx, 
                                     tag_array[blockA], 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));
    } else {
      if (rp == RRIP) { //RRIP
        status = srrip_replacement_policy(idx, 
                                     tag_array[blockA], 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));
      }
    }
  }
  expected_miss_hit = HIT_LOAD;
  EXPECT_EQ(result.miss_hit, expected_miss_hit); 
  EXPECT_EQ(status, 0);

  int tag_input;
  bool tagB_evicted = false;

  //while (!tagB_evicted) { 
  for (size_t i = 0; i < 20; i++) {
  
    //for (int i = 0; i < associativity; i++) {
      tag_input = rand()%4096;
      //if (tag_input == tag_array[i]) {
    //     tag_input = rand()%4096;
    //   } 
    // }
    printf("tag ran: %i \n", tag_input);
    loadstore = 0;

    if (rp == LRU) { //LRU
    status = lru_replacement_policy(idx, 
                                     tag_input, 
                                     associativity,
                                     loadstore,
                                     cache_line[setB],
                                     &result,
                                     bool(debug_on));

    } else {
      if (rp == NRU) { //En caso de usarse rp NRU
        status = nru_replacement_policy(idx, 
                                     tag_input, 
                                     associativity,
                                     loadstore,
                                     cache_line[setB],
                                     &result,
                                     bool(debug_on));
      } else {
        if (rp == RRIP) { //RRIP
          status = srrip_replacement_policy(idx, 
                                     tag_input, 
                                     associativity,
                                     loadstore,
                                     cache_line[setB],
                                     &result,
                                     bool(debug_on));
       }
      }
   }
    EXPECT_EQ(status, OK);
     printf("adrees out: %i \n", result.evicted_address);
    if (result.evicted_address == tag_array[blockB]) {
      tagB_evicted = true;
      EXPECT_EQ(result.dirty_eviction, false);
    }   
  }

  bool tagA_evicted = false;

  while (!tagA_evicted) { 

    for (int i = 0; i < associativity; i ++) {
      tag_input = rand()%4096;
      if (tag_input == tag_array[i]) {
        i = 0;
      } 
    }

    loadstore = 0;

    if (rp == LRU) { //LRU
    status = lru_replacement_policy(idx, 
                                     tag_input, 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));

    } else {
      if (rp == NRU) { //En caso de usarse rp NRU
        status = nru_replacement_policy(idx, 
                                     tag_input, 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));
      } else {
        if (rp == RRIP) { //RRIP
          status = srrip_replacement_policy(idx, 
                                     tag_input, 
                                     associativity,
                                     loadstore,
                                     cache_line[setA],
                                     &result,
                                     bool(debug_on));
        }
      }
    }
    EXPECT_EQ(status, OK);
    if (result.evicted_address == tag_array[blockA]) {
      tagA_evicted = true;
      EXPECT_EQ(result.dirty_eviction, true);
    }   
  }
}


/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST(L1cache, boundaries){
  int rp = rand()%3;
  int status;
  bool loadstore;
  debug_on = 1;
  int idx = rand()%1024;
  int tag = rand()%4096;
  int associativity = 1 << (rand()%4);

  int idx_invalid = idx*-1;
  int tag_invalid = tag*-1;
  int associativity_invalid;
  struct operation_result result;

  if (associativity == 1) {
    associativity_invalid = associativity + 2;
  } else {
    associativity_invalid = associativity + 1;
  }

  //Prueba con un valor inválido de index

  if (rp == LRU) {
      struct entry cache_line[associativity];

      status = lru_replacement_policy(idx_invalid, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
  } else {
    if (rp == NRU) { //NRU
   
      struct entry cache_line[associativity];

      status = nru_replacement_policy(idx_invalid, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    } else {
      if (rp == RRIP) { //SRRIP
   
        struct entry cache_line[associativity];

        status = srrip_replacement_policy(idx_invalid, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      }
    }
  }
  EXPECT_EQ(status, ERROR);

  //Prueba con un valor inválido de tag

  if (rp == LRU) {
      struct entry cache_line[associativity];

      status = lru_replacement_policy(idx, 
                                     tag_invalid, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
  } else {
    if (rp == NRU) { //NRU
   
      struct entry cache_line[associativity];

      status = nru_replacement_policy(idx, 
                                     tag_invalid, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    } else {
      if (rp == RRIP) { //SRRIP
   
        struct entry cache_line[associativity];

        status = srrip_replacement_policy(idx, 
                                     tag_invalid, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      }
    }
  }
  EXPECT_EQ(status, ERROR);

  //Prueba con un valor inválido de index

  if (rp == LRU) {
      struct entry cache_line[associativity_invalid];

      status = lru_replacement_policy(idx_invalid, 
                                     tag, 
                                     associativity_invalid,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
  } else {
    if (rp == NRU) { //NRU
   
      struct entry cache_line[associativity_invalid];

      status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity_invalid,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    } else {
      if (rp == RRIP) { //SRRIP
   
        struct entry cache_line[associativity_invalid];

        status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity_invalid,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
      }
    }
  }
  EXPECT_EQ(status, ERROR);
}

/* 
 * Gtest main function: Generates random seed, if not provided,
 * parses DEBUG flag, and execute the test suite
 */
int main(int argc, char **argv) {
  int argc_to_pass = 0;
  char **argv_to_pass = NULL; 
  int seed = 0;

  /* Generate seed */
  seed = time(NULL) & 0xffff;

  /* Parse arguments looking if random seed was provided */
  argv_to_pass = (char **)calloc(argc + 1, sizeof(char *));
  
  for (int i = 0; i < argc; i++){
    std::string arg = std::string(argv[i]);

    if (!arg.compare(0, 20, "--gtest_random_seed=")){
      seed = atoi(arg.substr(20).c_str());
      continue;
    }
    argv_to_pass[argc_to_pass] = strdup(arg.c_str());
    argc_to_pass++;
  }

  /* Init Gtest */
  ::testing::GTEST_FLAG(random_seed) = seed;
  testing::InitGoogleTest(&argc, argv_to_pass);

  /* Print seed for debug */
  printf(YEL "Random seed %d \n",seed);
  srand(seed);

  /* Parse for debug env variable */
  get_env_var("TEST_DEBUG", &debug_on);

  /* Execute test */
  return RUN_ALL_TESTS();
  
  /* Free memory */
  free(argv_to_pass);

  return 0;
}
