/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>


#ifndef TESTING
#define TESTING

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size) {
   *offset_size = log2 (blocksize_bytes);
   *idx_size = log2((cachesize_kb*KB)/(associativity*blocksize_bytes));
   *tag_size = ADDRSIZE - *idx_size - *offset_size;
   return OK;
}

void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{
unsigned int indexMask = address;
unsigned int tagMask = address;

indexMask = indexMask<<tag_size;
*idx = indexMask>>(tag_size+offset_size);
tagMask = tagMask>>(idx_size+offset_size);
*tag = tagMask;

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{

   if((associativity%2 == 1 && associativity != 1) || associativity == 0) {
      return ERROR;
   }  
   if(tag < 0 || idx < 0) {
      return ERROR;
   } 


   // M: intervalo de Re-referencia
   int M;
   if (associativity <= 2) {
      M = 1; // si estoy usando NRU, M deberia
   } else {
      M = 2;
   }
   // cout << "M = " << M << endl;

   bool gotHit = false;
   int hit_way;
   int evicted_way;
   int rp_value_reg[associativity];


   //revisa los ways
   // cout << "index: " << idx << endl;
   for (int i = 0; i < associativity; i++)
   {
      // cout << "i: " << i << endl;
      // gotHit = false;
      rp_value_reg[i] = cache_blocks[i].rp_value; //guardo en un array los valores de los rp_values (me sirve para el miss)
      
      // cout << "rp_value_reg[" << i << "]: " << rp_value_reg[i] << " / tag: " << cache_blocks[i].tag << endl;
      if(cache_blocks[i].tag == tag && cache_blocks[i].valid){
         gotHit = true;
         hit_way = i; //guardo en hit_way el way en que hubo hit
         result->evicted_address = hit_way; //cambio
      }
   }

   if(gotHit){
      // cout << "HIT!!!" << endl;
      
/////////////////////////////////// HIT

      //Resultados (tipo de hit, dirty eviction y evicted address)
      if (loadstore){
         result->miss_hit = HIT_STORE;
         cache_blocks[hit_way].dirty = true;
         result->dirty_eviction = false;
         // result->evicted_address = NULL;
      } else {
         result->miss_hit = HIT_LOAD;
         result->dirty_eviction = false;
         // result->evicted_address = NULL;
      }

      // Cambio de los valores de RRIP
      cache_blocks[hit_way].rp_value = 0;
   }
   else {

//////////////////////////// MISS


   //resultados
      // cout << "MISS!!!" << endl;
      // cout << "loadstore: " << loadstore << endl;
      if (loadstore){
         // cout << "test" << endl;
         result->miss_hit = MISS_STORE;  
      } else {
         result->miss_hit = MISS_LOAD;
      }

      bool found3 = false;
      

      //no se encontro 3: todos los valores aumentan
      while(!found3){
         //busca 2^m -1
         for(int k = 0; k<associativity; k++){
            // cout << pow(2,M)-1 << endl;
            if (cache_blocks[k].rp_value == pow(2,M)-1){
               found3 = true;
               evicted_way = k;
               break;
               cout << "test break" << endl;
            }
         }
         if(!found3){
            //aumenta todos si no encuentra 3
            for (int j = 0; j < associativity; j++){
               // cout << pow(2,M)-1 << endl;
               if(cache_blocks[j].rp_value<pow(2,M)-1){
                  cache_blocks[j].rp_value++;
               }
            }
         }
         
      }


      // tengo en    evicted_way    el way que se va a sacar de cache
      //sustitucion del bloque: 
      // resultado del dirty eviction
      if(cache_blocks[evicted_way].dirty){
         result->dirty_eviction = true;
      } else{
         result->dirty_eviction = false;
      }

      // sustitucion del bloque
      result->evicted_address = cache_blocks[evicted_way].tag; //cambio
      cache_blocks[evicted_way].tag = tag;
      cache_blocks[evicted_way].valid = true;
      cache_blocks[evicted_way].rp_value = (pow(2, M) -2); //ingresa con 2^M -2 (valor lejano)
      // cambiar este para NRU, ingresa con 1
      if(loadstore){
         cache_blocks[evicted_way].dirty = true;
      } else{
         cache_blocks[evicted_way].dirty = false;
      }

   }
   // FALTA!!
   //    evicted address
   //    logica del valid
   return OK;
}






int nru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if((associativity%2 == 1 && associativity != 1) || associativity == 0) {
      return ERROR;
   }  
   if(tag < 0 || idx < 0) {
      return ERROR;
   } 
   
   // M: intervalo de Re-referencia
   int M = 1;

   bool gotHit = false;
   int hit_way;
   int evicted_way;
   int rp_value_reg[associativity];


   //revisa los ways
   for (int i = 0; i < associativity; i++)
   {
      rp_value_reg[i] = cache_blocks[i].rp_value; //guardo en un array los valores de los rp_values (me sirve para el miss)
      
      if(cache_blocks[i].tag == tag && cache_blocks[i].valid){
         gotHit = true;
         hit_way = i; //guardo en hit_way el way en que hubo hit
         result->evicted_address = hit_way; //cambio
      }
   }

   if(gotHit){
/////////////////////////////////// HIT

      //Resultados (tipo de hit, dirty eviction y evicted address)
      if (loadstore){
         result->miss_hit = HIT_STORE;
         result->dirty_eviction = false;
         cache_blocks[hit_way].dirty = true;
      } else {
         result->miss_hit = HIT_LOAD;
         result->dirty_eviction = false;
      }

      // Cambio de los valores de RRIP
      cache_blocks[hit_way].rp_value = 0;
   }
   else {

//////////////////////////// MISS

   //resultados
     
      if (loadstore){
         result->miss_hit = MISS_STORE;   ///// SEG FAULT!!!
         //result->dirty_eviction = true;
      } else {
         result->miss_hit = MISS_LOAD;
      }

      bool found1 = false;
      

      //no se encontro 1: todos los valores aumentan
      while(!found1){
         //busca 2^m -1
         for(int k = 0; k<associativity; k++){
            // cout << pow(2,M)-1 << endl;
            // cout << "blaaa" << (int)cache_blocks[k].rp_value;
            if (cache_blocks[k].rp_value == 1){
               found1 = true;
               evicted_way = k;
               if(cache_blocks[k].dirty == true) {
                  result->dirty_eviction =true;
               } else {
                  result->dirty_eviction =false;
               }
               break;
            }
         }
         if(!found1){
            //aumenta todos si no encuentra 1
            for (int j = 0; j < associativity; j++){
               // cout << pow(2,M)-1 << endl;
               if(cache_blocks[j].rp_value == 0){
                  cache_blocks[j].rp_value++;
               }
            }
         }
         
      }


      // sustitucion del bloque
      result->evicted_address = cache_blocks[evicted_way].tag; //cambio
      cache_blocks[evicted_way].tag = tag;
      cache_blocks[evicted_way].valid = true;
      cache_blocks[evicted_way].rp_value = 0; //ingresa con 2^M -2 (valor lejano)

      if(loadstore){
         cache_blocks[evicted_way].dirty = true;
      } else{
         cache_blocks[evicted_way].dirty = false;
      }
   }

   return OK;
}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if((associativity%2 == 1 && associativity != 1) || associativity == 0) {
      return ERROR;
   }  
   if(tag < 0 || idx < 0) {
      return ERROR;
   } 


   bool gotHit = false;
   int hit_way;
   int evicted_way = 0;
   int rp_value_reg[associativity];


   //revisa los ways
   // cout << "index: " << idx << endl;
   for (int i = 0; i < associativity; i++)
   {
      // cout << "i: " << i << endl;
      // gotHit = false;
      rp_value_reg[i] = cache_blocks[i].rp_value; //guardo en un array los valores de los rp_values (me sirve para el miss)
      
      // cout << "rp_value_reg[" << i << "]: " << rp_value_reg[i] << " / tag: " << cache_blocks[i].tag << endl;
      if(cache_blocks[i].tag == tag && cache_blocks[i].valid){
         gotHit = true;
         hit_way = i; //guardo en hit_way el way en que hubo hit
         result->evicted_address = hit_way; //cambio
            
      }
   }
   
   if(gotHit){
      // cout << "HIT!!!" << endl;
      
/////////////////////////////////// HIT

      //Resultados (tipo de hit, dirty eviction y evicted address)
      if (loadstore){
         result->miss_hit = HIT_STORE;
         cache_blocks[hit_way].dirty = true;
      } else {
         result->miss_hit = HIT_LOAD;
      }

      // Cambio de los valores de RRIP
      cache_blocks[hit_way].rp_value = 0;

      for(int j = 0; j<associativity; j++){
         if((int)cache_blocks[j].rp_value < (int)cache_blocks[hit_way].rp_value){
            cache_blocks[j].rp_value++;
         }
      }
   }
   else {
      

//////////////////////////// MISS


   //resultados
      // cout << "MISS!!!" << endl;
      if (loadstore){
         result->miss_hit = MISS_STORE;   
         result->dirty_eviction = true;
      } else {
         result->miss_hit = MISS_LOAD;
      }

      for (int i = 0; i < associativity; i++)
      {
         
         if(cache_blocks[i].rp_value == associativity-1){
            evicted_way = i;
         }
         else{
            cache_blocks[i].rp_value++;
         }
      }
      

      


      // tengo en    evicted_way    el way que se va a sacar de cache
      //sustitucion del bloque: 
      // resultado del dirty eviction
      if(cache_blocks[evicted_way].dirty){
         result->dirty_eviction = true;
      } else{
         result->dirty_eviction = false;
      }

      // sustitucion del bloque
      result->evicted_address = cache_blocks[evicted_way].tag; //cambio
      cache_blocks[evicted_way].tag = tag;
      cache_blocks[evicted_way].valid = true;
      cache_blocks[evicted_way].rp_value = 0; 

      if(loadstore){
         cache_blocks[evicted_way].dirty = true;
      } else{
         cache_blocks[evicted_way].dirty = false;
      }

   }

   return OK;
}


#endif
