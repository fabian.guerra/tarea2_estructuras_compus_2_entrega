#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>


using namespace std;
#define KB 1024
/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char** argv) {
  
  ///////////////////// RECEPCION DE ARGUMENTOS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  //Variables donde se guardaran los argumentos de entrada
    int cacheSize;
    int blockSize;
    int associativity; 
    int replacementPolicy;
    string salida;
    string options[5] = {"-t","-l","-a","-rp"};

    //Recorre los argumentos de entrada
    for (int i = 0; i < 9; i++)
    {
        //Si el argumento es -t guarda el siguiente valor en CacheSize
        if (argv[i] == options[0])
        {
            cacheSize = atoi(argv[i+1]);
        }

        //Si el argumento es -l guarda el siguiente valor en BlockSize
        if (argv[i] == options[1])
        {
            blockSize = atoi(argv[i+1]);
        }
        
        //Si el argumento es -a guarda el siguiente valor en Associativity
        if (argv[i] == options[2])
        {
            associativity = atoi(argv[i+1]);
        }

        //Si el argumento es -rp guarda el siguiente valor en ReplacementPolicy
        if (argv[i] == options[3])
        {
            replacementPolicy = atoi(argv[i+1]);
        }
    }

  /* Get trace's lines and start your simulation */
    
    int byteOffsetSize;
    int indexSize;
    int tagSize;
    int state;
    int address;
    int index;
    int tag;

    int dirty_evictions = 0;
    int totalHit = 0;
    int loadHit = 0;
    int storeHit = 0;
    int totalMiss = 0;
    int loadMiss = 0;
    int storeMiss = 0;
    // int seed = 0;
    bool loadStore = false;
    bool gotHit = false;
    bool alreadyLoadedOnMiss = false;
    int currentInsCounter = 0;
    int totalInsCounter = 0;
    string datos;
    string replacement_policy_string;

    state = field_size_get(cacheSize,
                    associativity,
                    blockSize,
                    &tagSize,
                    &indexSize,
                    &byteOffsetSize);
    // seed = time(NULL) & 0xffff;
    // srand(seed);

    if (replacementPolicy >= 3){
      return ERROR;
    }
    if (replacementPolicy == 2){
      replacement_policy_string = "SRRIP";
    }
    if (replacementPolicy == 1){
      replacement_policy_string = "NRU";
    }
    if (replacementPolicy == 0){
      replacement_policy_string = "LRU";
    }

    cout << "---------------------------" << endl;
    cout << "Tamaño de la caché: " << cacheSize << "KB \n";
    cout << "Asociatividad: " << associativity << " ways \n";
    cout << "Tamaño del bloque: " << blockSize << "bits \n";
    cout << "---------------------------" << endl;
    cout << "Bits de tag: " << tagSize << " bits \n";
    cout << "Bits de index: " << indexSize << " bits \n";
    cout << "Bits de offset: " << byteOffsetSize << " bits \n";
    cout << "Replacement policy: " << replacement_policy_string << " \n";
    cout << "---------------------------" << endl;
  
    

    //Caché

    // struct entry cache[(cacheSize*KB)/(associativity*blockSize)][associativity];
    
    entry cache[(cacheSize*KB)/(associativity*blockSize)][associativity];
    operation_result op; // struct de resultado
    op.miss_hit = MISS_STORE;
    op.dirty_eviction = false;
    op.evicted_address = 0;
    int initial_rp = 0;

    // valores iniciales de rp_value
    if (replacementPolicy == 2){
      initial_rp = 3;
    }
    if (replacementPolicy == 1){
      initial_rp = 1;
    }
    if (replacementPolicy == 0){
      initial_rp = associativity-1;
    }
    // cout << "ass " << associativity << endl;
    if(associativity <= 2 && replacementPolicy == 2){
      // cout << "success " << endl;
      initial_rp = 1;
    }

    for (int i = 0; i < (cacheSize*KB)/(associativity*blockSize); i++)
    {
      for (int j = 0; j < associativity; j++)
      {
        cache[i][j].valid = false;
        cache[i][j].dirty = false;
        cache[i][j].tag = 0;
        // cout << initial_rp << endl;
        cache[i][j].rp_value = initial_rp;
        if(replacementPolicy == 0){
          if (initial_rp != -1){
          initial_rp--;
          if(initial_rp == -1){
            initial_rp = associativity-1;
          }
        }
        }
        
        // cout << " rp value " << (int)cache[i][j].rp_value << endl;
        // cout << " tag " << cache[i][j].tag << endl;
        // cout << " valid " << cache[i][j].valid << endl;
        // cout << " dirty " << cache[i][j].dirty << endl;
        // cout << " amount of sets: " << (cacheSize*KB)/(associativity*blockSize) << endl;
      }
    }
    
    while (cin) {
    // for (int i = 0; i < 500; i++)
    // {
        cin >> datos;
        if (datos == "1") {
          break;
        }

        // cout << datos << endl;
        cin >> datos;
        // cout << datos << endl;
        
        if (datos == "0") {
          loadStore = false;
        }
        if (datos == "1") {
          loadStore = true;
        }
        
        cin >> datos; 
        // cout << datos << endl;

        address = stol(datos, nullptr, 16);

        cin >> datos; 

        currentInsCounter = stol(datos, NULL, 10);
        totalInsCounter = totalInsCounter + currentInsCounter;
        
        address_tag_idx_get(address,
                        tagSize,
                        indexSize,
                        byteOffsetSize,
                        &index,
                        &tag);

// cout << datos << endl;
        // cout << "Address: " << hex << address << " \n";
        // cout << "Tag: " << hex << tag << " \n";
        // cout << "Index: " << hex << index << " \n";

      switch (replacementPolicy)
      {
        case 0: ////////////////////////// LRU
        
          lru_replacement_policy(index, tag, associativity, loadStore,cache[index], &op, false);
          if(op.miss_hit == MISS_LOAD){
            totalMiss ++;
            loadMiss ++;
            if(op.dirty_eviction){
              dirty_evictions++;
            }
          }
          if(op.miss_hit == MISS_STORE){
            totalMiss ++;
            storeMiss ++;
            if(op.dirty_eviction){
              dirty_evictions++;
            }
          }
          if(op.miss_hit == HIT_LOAD){
            totalHit ++;
            loadHit ++;
          }
          if(op.miss_hit == HIT_STORE){
            totalHit ++;
            storeHit ++;
          }
          break;

      
        case 1: ///////////////////////////////////////////// NRU

          nru_replacement_policy(index, tag, associativity, loadStore,cache[index], &op, false);
          if(op.miss_hit == MISS_LOAD){
            totalMiss ++;
            loadMiss ++;
            if(op.dirty_eviction){
              dirty_evictions++;
            }
          }
          if(op.miss_hit == MISS_STORE){
            totalMiss ++;
            storeMiss ++;
            if(op.dirty_eviction){
              dirty_evictions++;
            }
          }
          if(op.miss_hit == HIT_LOAD){
            totalHit ++;
            loadHit ++;
          }
          if(op.miss_hit == HIT_STORE){
            totalHit ++;
            storeHit ++;
          }
          break;


        case 2:  //////////////////////////////////////////// RRIP


          
          // cout << "SRRIP REPLACEMENT POLICY DEBUG" <<  endl;
          // cout << (int)cache[index][0].rp_value << endl;
          srrip_replacement_policy(index, tag, associativity, loadStore,cache[index], &op, false);
          if(op.miss_hit == MISS_LOAD){
            totalMiss ++;
            loadMiss ++;
            if(op.dirty_eviction){
              dirty_evictions++;
            }
          }
          if(op.miss_hit == MISS_STORE){
            totalMiss ++;
            storeMiss ++;
            if(op.dirty_eviction){
              dirty_evictions++;
            }
          }
          if(op.miss_hit == HIT_LOAD){
            totalHit ++;
            loadHit ++;
            
          }

          if(op.miss_hit == HIT_STORE){
            totalHit ++;
            storeHit ++;
          }
          break;
    
        default:
          break;
      }
    } 
    
  /* Print cache configuration */

  /* Print Statistics */
  cout << "dirty evictions: " << dirty_evictions << endl;
  cout << "Load Misses: " << loadMiss << endl;
  cout << "Store Misses: " << storeMiss << endl;
  cout << "Total Misses: " << totalMiss << endl;

  cout << "Load hits: " << loadHit << endl;
  cout << "Store hits: " << storeHit << endl;
  cout << "Total hits: " << totalHit << endl;
  cout << "Overall miss rate: " << (double)((double)totalMiss/(double)(totalMiss+totalHit)) << endl;
  cout << "Read miss rate: " << (double)((double)loadMiss/(double)(loadMiss+loadHit)) << endl;

return 0;
}
